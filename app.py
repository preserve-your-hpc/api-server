import os
import json

from flask import Flask, request, render_template, send_from_directory, send_file
from flask_restful import Resource, Api, reqparse
from werkzeug.utils import secure_filename

from managers.db_manager import DBManager
from citar_celery.tasks import create_docker, upload_container

class PrefixMiddleware(object):

    def __init__(self, app, prefix=''):
        self.app = app
        self.prefix = prefix

    def __call__(self, environ, start_response):

        if environ['PATH_INFO'].startswith(self.prefix):
            environ['PATH_INFO'] = environ['PATH_INFO'][len(self.prefix):]
            environ['SCRIPT_NAME'] = self.prefix
            return self.app(environ, start_response)
        else:
            start_response('404', [('Content-Type', 'text/plain')])
            return ["This url does not belong to the app.".encode()]

UPLOAD_FOLDER = 'images/uploads'
IMAGES_FOLDER = 'images'
SERVER_ADDRESS = 'http://134.60.51.72/'

project_root = os.path.dirname(__file__)
template_path = os.path.join(project_root, 'templates')

app = Flask(__name__, template_folder=template_path)
app.config["APPLICATION_ROOT"] = "/restapi/"
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['IMAGES_FOLDER'] = IMAGES_FOLDER
# app.wsgi_app = PrefixMiddleware(app.wsgi_app, prefix='/restapi')
api = Api(app)

def sizeof_fmt(num, suffix='B'):
    for unit in ['','Ki','Mi','Gi','Ti','Pi','Ei','Zi']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)

class SaveJob(Resource):
    def get(self):
        print('hey')
        return 'Get Request.'

    def post(self):

        parser = reqparse.RequestParser()
        parser.add_argument('job_id', location='form')
        parser.add_argument('data', location='form')
        parser.add_argument('modules', location='form')
        args = parser.parse_args()
        print(parser.parse_args())

        d = DBManager()
        if d.get_job_by_job_id(job_id=args['job_id']):
            return {'text': 'Already exists'}

        dir = "{}/{}".format(app.config['UPLOAD_FOLDER'], args['job_id'])
        os.mkdir(dir)
        for key, file in request.files.items():
            print(file)
            if file:
                filename = secure_filename(file.filename)
                file.save(os.path.join(dir, filename))

        print(args['modules'])
        print(json.loads(args['data']).get('command'))
        d.insert_job(job_id=args['job_id'],
                 libraries=json.loads(args['modules']),
                 command=json.loads(args['data']).get('command'),
                 data=json.loads(args['data']).get('data'))

        if d.get_job_by_job_id(job_id=args['job_id']):
            return {'link': 'http://134.60.152.82/jobs/{}/'.format(args['job_id'])}

        return {'text': 'Done'}


class CreateContainer(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('id', location='form')
        parser.add_argument('libs', location='form')
        parser.add_argument('job_id', location='form')
        parser.add_argument('uid', location='form')
        parser.add_argument('container_type', location='form')
        args = parser.parse_args()
        print(args['uid'])
        create_docker.delay(args['uid'], args['container_type'])
        print('done')
        return {'text': 'Done'}

class DownloadContainer(Resource):
    def get(self, uid):
        print(uid)
        d = DBManager()
        cur = d.get_container_path(uid=uid)[0]
        print(cur)
        if cur:
            return send_file(filename_or_fp=cur, as_attachment=True)


class ExportContainerToEAAS(Resource):
    def post(self):
        # {'author': 'ku',
        # 'title': 'test_title',
        # 'runtime_type': 'singularity',
        # 'process_args': 'ls',
        # 'env_id': '',
        # 'process_env': '',
        # 'description': 'test_descr',
        # 'output_folder': '/output',
        # 'image_type': 'simg',
        # 'image_url': '',
        # 'task_id': '',
        # 'input_folder': '/input',
        # 'image_name': '',
        # 'uid': 'a1566ae4-3138-4087-b2f8-f55b118f5752'}

        parser = reqparse.RequestParser()
        parser.add_argument('uid', location='form')

        parser.add_argument('author', location='form')
        parser.add_argument('title', location='form')
        parser.add_argument('runtime_type', location='form')
        parser.add_argument('process_args', location='form')
        parser.add_argument('process_env', location='form')
        parser.add_argument('description', location='form')
        parser.add_argument('output_folder', location='form')
        parser.add_argument('image_type', location='form')
        parser.add_argument('input_folder', location='form')
        parser.add_argument('image_name', location='form')
        args = parser.parse_args()
        print(args)
        upload_container.delay(args)
        print('done')
        return {'text': 'Done'}


api.add_resource(SaveJob, '/restapi/save-job/')
api.add_resource(CreateContainer, '/restapi/create-container/')
api.add_resource(DownloadContainer, '/restapi/download-container/<string:uid>')
api.add_resource(ExportContainerToEAAS, '/restapi/export-container/')

if __name__ == '__main__':
    app.run(debug=True)
    # app.run()
