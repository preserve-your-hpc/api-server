from __future__ import absolute_import
from celery import Celery

app = Celery('citar_celery',
             broker='amqp://citar:citar@localhost/citar',
             backend='amqp://citar:citar@localhost/citar',
             include=['citar_celery.tasks'])
# 
# app.conf.task_routes = ([
#     ('*', {'queue': 'create'}),
#     # ('citar_celery.tasks.import_container', {'queue': 'upload'}),
# ],)

# from celery.task.schedules import crontab
# from celery.decorators import periodic_task
# @periodic_task(run_every=crontab(minute=1))
# def every_1():
#     print("This runs ")
