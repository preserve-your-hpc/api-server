import time

from citar_celery.celery import app
from managers.container_manager import DockerManager
from managers.email_manager import EmailManager
from managers.import_manager import ImportManager
from managers.db_manager import DBManager

@app.task
def create_docker(job_id, container_type):
    print('create_container_task')
    d = DockerManager(job_id, container_type)
    d.create_image()

@app.task
def send_mail(job_id):
    em = EmailManager(job_id)
    em.send_mail()
    # d = DockerManager(job_id)
    # d.create_image()

@app.task
def upload_container(args):
    print('upload_container_task')
    im = ImportManager()
    db = DBManager()
    file_name = db.get_current(args['uid'])[6]
    im.load_data(file_name=file_name,
                 image_type='simg',
                 image_name=args['image_name'],
                 runtime_type=args['runtime_type'],
                 author=args['author'],
                 description=args['description'],
                 title=args['title'],
                 process_args=args['process_args'],
                 process_env=args['process_env'],
                 input_folder=args['input_folder'],
                 output_folder=args['output_folder']
                )
    im.run()
    # d = DockerManager(job_id)
    # d.create_image()
