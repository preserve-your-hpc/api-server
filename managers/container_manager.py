import docker
import os
import random
import subprocess

from managers.db_manager import DBManager
from managers.email_manager import EmailManager


class DockerManager():

    def __init__(self, guid, container_type):
        # self.client = docker.APIClient(base_url='unix://var/run/docker.sock', timeout=600)
        self.client = docker.APIClient(base_url='unix://var/run/docker.sock', timeout=600)
        self.db_con = DBManager()
        self.guid = str(guid)
        self.base_dir = 'images'
        self.dockerfile = '{}/{}_df'.format(self.base_dir, self.guid)
        self.data = []
        self.libs = []

    def _get_data(self):
        self.data = self.db_con.get_job_data(self.guid)
        self.libs = self.db_con.get_libraries_directories(self.guid)
        self.email = self.db_con.get_current(self.guid)[3]
        self.container_type = self.db_con.get_current(self.guid)[8]

    def _create_basedir(self):
        if not os.path.isdir(self.base_dir):
            os.mkdir(self.base_dir)

    def _create_dockerfile(self):
        print(self.data)
        base_file = """
            FROM centos_dev
            RUN printf 'cd /experiment \\n{0}' >> /run.sh
            WORKDIR /experiment
            COPY uploads/{1}/* ./
            """.format(self.data[3].strip('"'),self.data[2])
        for lib in self.libs:
            print(lib[0])
            base_file += """
                {}""".format(lib[0])

        with open('{}/{}_df'.format(self.base_dir, self.guid), 'w+') as f:
            f.write(base_file)
        f.close()
        print('created DOckerfile {}'.format(self.guid))


    def _create_container(self):
        print('start to build {}'.format(self.guid))
        try:
            response = [line for line in self.client.build(path=self.base_dir, tag=self.guid, dockerfile='{}_df'.format(self.guid))]
            print(response)
        except Exception as ex:
            print('error on creation container {}'.format(ex))
        else:
            print('created {}'.format(self.guid))


    def _export_docker(self):
        print('start to export to .tar ')
        with open('{}/{}.tar'.format(self.base_dir, self.guid), 'wb') as f:
            for chunk in self.client.get_image(self.guid):
                f.write(chunk)
        print('saved {}'.format(self.guid))
        return os.path.abspath("{}/{}.tar".format(self.base_dir, self.guid))


    def _export_singularity(self):
        print('start to export to .simg ')

        self.client.tag('{}'.format(self.guid), 'localhost:5000/{}'.format(self.guid))
        self.client.push('localhost:5000/{}'.format(self.guid))

        text = """Bootstrap: docker
Registry: http://localhost:5000
Namespace:
From: {}:latest""".format(self.guid)

        with open('{}/{}_def'.format(self.base_dir, self.guid), 'w+') as f:
            f.write(text)
            print('done {}'.format(self.base_dir))
        f.close()

        bashCommand = "sudo SINGULARITY_NOHTTPS=1 singularity build {0}/{1}.simg {0}/{1}_def".format(self.base_dir, self.guid)
        process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
        output, error = process.communicate()
        print(output, error)
        return os.path.abspath("{0}/{1}.simg".format(self.base_dir, self.guid))


    def create_image(self):
        self._get_data()
        self._create_basedir()
        self._create_dockerfile()

        try:
            self._create_container()
            path = ''
            if self.container_type == "2":
                path = self._export_singularity()
            elif self.container_type == "1":
                path = self._export_docker()
            else:
                print('nothing')

        except Exception as e:
            self.db_con.update(self.guid, status='3')
            print(e)
        else:
            self.db_con.update(self.guid, status='2', url=path)

            em = EmailManager(self.guid, self.email)
            em.send_mail()
