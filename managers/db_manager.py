from sqlalchemy import create_engine, MetaData, Table
from sqlalchemy.orm import mapper, sessionmaker

from sqlalchemy_utils import database_exists, create_database

import json

import os, sys
parentPath = os.path.abspath("..")
if parentPath not in sys.path:
    sys.path.insert(0, parentPath)

class DBManager():

    def __init__(self):
	user_name = ""
	password = ""
	db_name = ""
        engine = create_engine('postgresql://{0):{1}@localhost:5432/{2}'.format(user_name, password, db_name))
        self.connection = engine.connect()


    def __del__(self):
        self.connection.close()
        pass

    def get_all(self):
        pass

    def get_current(self, uid):
        result = self.connection.execute('''
            SELECT id, created, modified, email, job_id, status_id, container_url, uid, container_type
            FROM container_history
            WHERE uid='{0}';'''.format(uid))

        if result.rowcount:
            return result.fetchone()


    def insert(self, job_id, data, libraries, command):
        pass


    def update(self, uid, *args, **kwargs):
        trans = self.connection.begin()
        try:
            s = kwargs.get('status', '3')
            u = kwargs.get('url', '')
        #
            self.connection.execute('''
                UPDATE container_history
                SET status_id={0}, container_url='{1}'
                WHERE uid='{2}';'''.format(s, u, uid))
            trans.commit()  # transaction is not committed yet
        except:
            trans.rollback() # this rolls back the transaction unconditionally
            raise


    def get_libraries_directories(self, uid):
        result = self.connection.execute('''
            select libraries.docker_directives FROM container_history
            INNER JOIN container_history_libs ON (container_history_libs.containercreationtask_id = container_history.id)
            INNER JOIN libraries ON (libraries.id = container_history_libs.library_id)
            where container_history.uid = '{0}';'''.format(uid))

        if result.rowcount:
            return result.fetchall()


    def get_job_data(self, uid):
        result = self.connection.execute('''
            select jobs.* FROM container_history
            INNER JOIN jobs ON (jobs.id = container_history.job_id)
            where container_history.uid = '{0}';'''.format(uid))

        if result.rowcount:
            return result.fetchone()

    def get_container_path(self, uid):
        result = self.connection.execute('''
            select container_url FROM container_history
            INNER JOIN jobs ON (jobs.id = container_history.job_id)
            where container_history.uid = '{0}';'''.format(uid))

        if result.rowcount:
            return result.fetchone()

    def get_job_by_job_id(self, job_id):

        result = self.connection.execute('''
            SELECT *
        	FROM jobs
        	where job_id = '{0}';'''.format(job_id))

        if result.rowcount:
            return result.fetchone()

    def insert_job(self, job_id, data, command, libraries):
        trans = self.connection.begin()
        try:
            self.connection.execute("""
                INSERT INTO jobs(
                data, job_id, command, libraries)
                VALUES ('{0}', {1}, '{2}', '{3}');""".format(json.dumps(data), job_id,  json.dumps(command), json.dumps(libraries)))
            trans.commit()  # transaction is not committed yet
        except:
            trans.rollback() # this rolls back the transaction unconditionally
            raise
