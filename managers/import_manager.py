import json
import requests
import string
import time
import os.path

upload_url = 'http://citar.eaas.uni-freiburg.de/emil/EmilContainerData/uploadUserInput'
import_url = 'http://citar.eaas.uni-freiburg.de/emil/EmilContainerData/importContainer'
check_url = 'http://citar.eaas.uni-freiburg.de/emil/EmilContainerData/taskState?taskId={}'
save_url = 'http://citar.eaas.uni-freiburg.de/emil/EmilContainerData/saveImportedContainer'
handle_url = 'http://citar.eaas.uni-freiburg.de/emil/handles/'
handle_value = 'http://citar.eaas.uni-freiburg.de:80/citar-headless-client/?id={}'

class ImportManager:

    def __init__(self):
        pass

    def load_data(self, file_name, image_name, runtime_type, image_type,
                  author='', description='', title='',
                  process_args=[], process_env=[],
                  input_folder='/input', output_folder='/output'):

        self.image_url = ''
        self.task_id = ''
        self.env_id = ''
        self.file_name = file_name
        self.image_name = image_name
        self.runtime_type = runtime_type
        self.process_args = process_args.split(' ')
        self.process_env = process_env.split(' ')
        self.input_folder = input_folder
        self.output_folder = output_folder
        self.image_type = image_type

        self.author = author
        self.description = description
        self.title = title

        # TODO: make 4th var
        if self.runtime_type == 'singularity':
            self.runtime_type = 2
        elif self.runtime_type == 'runc':
            self.runtime_type = 0
        elif self.runtime_type == 'docker':
            self.runtime_type = 1

        print("loaded")

    def load_data_from_json(self, data):

        self.file_name = self._get_or_input_console(data, "fileName")
        self.image_name = self._get_or_input_console(data, "imageName")

        if self._get_or_input_console(data, "runtimeType") == 'singularity':
            self.runtime_type = 2
        elif self._get_or_input_console(data, "runtimeType") == 'runc':
            self.runtime_type = 0
        elif self._get_or_input_console(data, "runtimeType") == 'docker':
            self.runtime_type = 1

        self.process_args = self._get_or_input_console(data, "processArgs").split(' ')
        self.process_env = self._get_or_input_console(data, "processEnv").split(' ')
        self.tag = self._get_or_input_console(data, "tag")
        self.input_folder = self._get_or_input_console(data, "inputFolder")
        self.output_folder = self._get_or_input_console(data, "outputFolder")
        self.image_type = self._get_or_input_console(data, "imageType")

        self.author = self._get_or_input_console(data,"author")
        self.description = self._get_or_input_console(data,"description")
        self.title = self._get_or_input_console(data,"title")

        if self._get_or_input_console(data,"handle") == "true":
            self.handle = True
        else:
            self.handle = False


    def _get_or_input_console(self, data, var_name):
        val = data.get(var_name, None)
        if val is None:
            val = input('enter {} please!: '.format(var_name))
        return val

    def run(self, handle=False):
        self._upload_container()
        self._import_container()
        self._save_container()

        # if self.handle:
        #     self._handle_container()

    def _upload_container(self):
        if not os.path.isfile(self.file_name):
            self.file_name = input('enter {} please!: '.format(var_name))
            self._upload_container(self)
            exit()

        files = {'file': open('%s' % self.file_name, 'rb')}
        upload_r = requests.post(upload_url, files=files)
        self.image_url = upload_r.json().get('userDataUrl')

    def _import_container(self):
        import_payload = {"urlString": self.image_url,
                          "runtimeID": self.runtime_type,
                          "name": self.image_name,
                          "tag":"",
                          "processArgs": self.process_args,
                          "inputFolder": self.input_folder,
                          "outputFolder": self.output_folder,
                          "imageType": self.image_type }
        if self.process_env == '':
            import_payload["processEnvs"] = None


        print(import_payload)
        import_r = requests.post(import_url, json=import_payload)
        print(import_r.status_code)
        self.task_id = import_r.json().get("taskId")

    def _save_container(self):
        while True:
            check_r = requests.get(check_url.format(self.task_id))
            if check_r.json().get('status') == 1:
                break
            if check_r.json().get('isDone'):
                self.env_id = check_r.json().get('userData').get('environmentId')
                requests.post(save_url,
                              json = {"id":self.env_id,
                                      "title":self.title,
                                      "description":self.description,
                                      "author":self.author}
                            )
                print(check_r.json().get('userData').get('environmentId'))
                break
            else:
                print('next loop')
                time.sleep(2)

    def _handle_container(self):
        handle_r = requests.post(handle_url,
                                 json={"handle":"11270/{}".format(self.env_id),
                                       "value":handle_value.format(self.env_id)}
                                )
